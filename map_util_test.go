package ds

import (
	"testing"
)

func TestGetKeysSorted(t *testing.T) {
	m := map[int]float32{
		1:  1.1,
		12: 6,
		2:  2.2,
		4:  4.5,
	}

	expect := []int{1, 2, 4, 12}
	keys := GetMapKeysSorted(m)

	for i := range expect {
		if expect[i] != keys[i] {
			t.Error(expect[i], keys[i])
		}
	}

	m2 := map[string]float32{
		"1":  1.1,
		"12": 6,
		"2":  2.2,
		"b":  4.5,
		"a":  4.5,
	}

	expect2 := []string{"1", "12", "2", "a", "b"}
	keys2 := GetMapKeysSorted(m2)

	for i := range expect2 {
		if expect2[i] != keys2[i] {
			t.Error(expect2[i], keys2[i])
		}
	}
}

func TestSortMapByValueA(t *testing.T) {
	m := map[int]float32{
		1:  1.1,
		12: 6,
		2:  2.2,
		4:  4.5,
	}

	keys := SortMapByValue(m, func(a, b float32) bool {
		return a < b
	})

	expect := []int{1, 2, 4, 12}
	if len(expect) != len(keys) {
		t.Error("lens differ ")
	}

	for i := range keys {
		if keys[i] != expect[i] {
			t.Error(keys[i], expect[i])
		}
	}
}

func TestSortMapByValueB(t *testing.T) {
	type A struct {
		s string
		a int
	}

	m := map[int]A{
		1: {"aa", 12},
		2: {"bbs", 1},
		3: {"a", -1},
	}

	keys := SortMapByValue(m, func(a, b A) bool {
		return a.s < b.s
	})

	expect := []int{3, 1, 2}

	if len(expect) != len(keys) {
		t.Error("lens differ ")
	}

	for i := range keys {
		if keys[i] != expect[i] {
			t.Error(keys[i], expect[i])
		}
	}
}

func TestSortMapByValueC(t *testing.T) {
	type A struct {
		s string
		a int
	}

	m := map[int]A{
		1: {"aa", 12},
		2: {"bbs", 1},
		3: {"a", -1},
	}

	keys := SortMapByValue(m, func(a, b A) bool {
		return a.a < b.a
	})

	expect := []int{3, 2, 1}

	if len(expect) != len(keys) {
		t.Error("lens differ ")
	}

	for i := range keys {
		if keys[i] != expect[i] {
			t.Error(keys[i], expect[i])
		}
	}
}
