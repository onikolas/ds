package ds

// A LIFO stack.
type Stack struct {
	data []any
	pos  int //index to place next push (Pos-1 is the last entry)
}

// Push data in the q.
func (s *Stack) Push(v any) {
	if s.pos == len(s.data) {
		s.data = append(s.data, v)
	}
	s.data[s.pos] = v
	s.pos++
}

// Remove and return topmost data.
func (s *Stack) Pop() any {
	if s.pos == 0 {
		return nil
	}
	s.pos--
	return s.data[s.pos]
}

// Get the top of the q without removing it.
func (s *Stack) Peek() any {
	if s.pos == 0 {
		return nil
	}
	return s.data[s.pos-1]
}

// Shrink reallocates the stack so it doesn't have unused space.
func (s *Stack) Shrink() {
	newData := make([]any, s.pos)
	copy(newData, s.data)
	s.data = newData
}
