package ds

import (
	"gitlab.com/onikolas/math"
)

// GridNode satisfies Node interface so it can be used for pathfinding. Grid can convert its cells
// to a GridNode using GetNode.
type GridNode[T any] struct {
	x, y int
	grid *Grid[T]
}

// Grid cells are uniquly identifiable by their x,y coords. We combine into one key using the index
// formula: y*width+x.
func (g *GridNode[T]) Key() NodeKey {
	return NodeKey(g.y*g.grid.width + g.x)
}

// Get the neighbours of this node. Will return 4 if grid is set to fourNeibourhood(4N) or 8 if set
// to eightNeibourhood(8N). Edges of the grid will return 3(4N)/5(8N) nodes and corners will return
// 2(4N)/3(8N) nodes.
func (g *GridNode[T]) Neighbours() []Node {
	cross := []math.Vector2[int]{{-1, 0}, {1, 0}, {0, -1}, {0, 1}}
	if g.grid.neighbourhood8 {
		corners := []math.Vector2[int]{{-1, -1}, {-1, 1}, {1, -1}, {1, 1}}
		cross = append(cross, corners...)
	}

	this := math.Vector2[int]{g.x, g.y}
	neighbours := []Node{}

	for _, v := range cross {
		neighbour := this.Add(v)
		if g.grid.InBounds(neighbour.X, neighbour.Y) && !g.grid.IsBlocked(neighbour.X, neighbour.Y) {
			neighbours = append(neighbours, &GridNode[T]{
				x:    neighbour.X,
				y:    neighbour.Y,
				grid: g.grid,
			})
		}
	}

	return neighbours
}

// Returns the cost of traveling from this current node to node. It is taken from the Cost() method
// of the grid data at the node's (destination's) location. If current and node are diagonal, the cost is
// multiplied by a diagonal cost multiplier (controlled with Grid.SetNeighbourHoodMode8).
func (g *GridNode[T]) Cost(node Node) int {
	n, ok := node.(*GridNode[T])
	if !ok {
		return -1
	}
	cost := math.Max(g.grid.GetCost(n.x, n.y), 1)
	vg := math.Vector2[int]{g.x, g.y}
	vn := math.Vector2[int]{n.x, n.y}
	if math.Diagonal(vg, vn) {
		// only approximates truth at for large costs..
		return int(float32(cost) * g.grid.diagonalCost)
	}
	return cost
}
