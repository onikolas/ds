package ds

import "testing"

func TestMappedSlice(t *testing.T) {
	ms := MappedSlice[int, string]{}.New()
	ms.AddMapped(123, "123")
	ms.AddSliceMapped([]int{2, 4, 6}, "2,4,6")
	ms.AddSliceMapped([]int{5, 5}, "5s")

	if a := ms.Get("5s"); a[0] != 5 || a[1] != 5 {
		t.Error(ms)
	}

	if a := ms.Get("123"); a[0] != 123 {
		t.Error(ms)
	}

	if a := ms.Get("2,4,6"); a[1] != 4 {
		t.Error(ms)
	}
}
