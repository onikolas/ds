package ds

import (
	"fmt"
	"strings"
)

// A generic grid for pathfinding.
type Grid[T any] struct {
	width, height  int
	cells          []T
	cellCosts      []int //negative costs represent blocked cells
	neighbourhood8 bool
	diagonalCost   float32
}

// Construct a new grid with given dimensions.
func NewGrid[T any](w, h int) *Grid[T] {
	return &Grid[T]{
		width:     w,
		height:    h,
		cells:     make([]T, w*h),
		cellCosts: make([]int, w*h),
	}
}

// Are the coords x,y valid.
func (g *Grid[T]) InBounds(x, y int) bool {
	if x < 0 || y < 0 || x >= g.width || y >= g.height {
		return false
	}
	return true
}

// 2d to 1d
func (g *Grid[T]) index(x, y int) int {
	return y*g.width + x
}

// 1d to 2d
func (g *Grid[T]) deIndex(i int) (x int, y int) {
	y = i / g.width
	x = i - y*g.width
	return
}

// Store data to grid cell x,y.
func (g *Grid[T]) Set(x, y int, data T) {
	if !g.InBounds(x, y) {
		return
	}
	g.cells[g.index(x, y)] = data
}

// Store travel cost to enter grid cell x,y.
func (g *Grid[T]) SetCost(x, y, cost int) {
	if !g.InBounds(x, y) {
		return
	}
	g.cellCosts[g.index(x, y)] = cost
}

// Set all grid cells to the same value.
func (g *Grid[T]) SetAll(data T) {
	for i := range g.cells {
		g.cells[i] = data
	}
}

// Set all travel costs to the same value.
func (g *Grid[T]) SetAllCosts(cost int) {
	for i := range g.cellCosts {
		g.cellCosts[i] = cost
	}
}

// Set grid cell to blocked. Blocked cells are ignored when pathfinding.
func (g *Grid[T]) SetBlocked(x, y int) {
	if !g.InBounds(x, y) {
		return
	}
	g.cellCosts[g.index(x, y)] = -1
}

// Set grid cell to unblocked. Provide a travel cost for the unblocked cell. Unblocked cells can be
// traversed when pathfinding.
func (g *Grid[T]) SetUnBlocked(x, y, cost int) {
	if !g.InBounds(x, y) {
		return
	}
	g.cellCosts[g.index(x, y)] = cost
}

// Check if cell is blocked (inaccessible to pathfinding).
func (g *Grid[T]) IsBlocked(x, y int) bool {
	if !g.InBounds(x, y) {
		return true
	}
	return g.cellCosts[g.index(x, y)] < 0
}

// Return grid contents at x,y.
func (g *Grid[T]) Get(x, y int) T {
	if !g.InBounds(x, y) {
		var zero T
		return zero
	}
	return g.cells[g.index(x, y)]
}

// Returns x,y from a NodeKey
func (g *Grid[T]) GetFromNodeKey(key NodeKey) (int, int) {
	return g.deIndex(int(key))
}

// Get cost needed to move into x,y.
func (g *Grid[T]) GetCost(x, y int) int {
	return g.cellCosts[g.index(x, y)]
}

// Convert a cell into a GridNode which can be used as input in the path search methods.
func (g *Grid[T]) GetNode(x, y int) GridNode[T] {
	return GridNode[T]{x: x, y: y, grid: g}
}

// Set neighbourhood mode to 4 neighbours (top, bot, left, right). Cost of traveling to neighbours
// is the whatever the Cost() of that cell is.
func (g *Grid[T]) SetNeighbourHoodMode4() {
	g.neighbourhood8 = false
}

// Set neighbourhood mode to 8 neighbours (top, bot, left, right, top-left, top-right, bot-left,
// bot-right). When moving diagonally, cost is calculated as cell cost multiplied by dcost
// (typically sqrt(2)).
func (g *Grid[T]) SetNeighbourHoodMode8(dcost float32) {
	g.diagonalCost = dcost
	g.neighbourhood8 = true
}

func (g *Grid[T]) String() string {
	str := strings.Builder{}
	for y := 0; y < g.height; y++ {
		for x := 0; x < g.width; x++ {
			fmt.Fprint(&str, g.Get(x, y), " ")
		}
		fmt.Fprintln(&str)
	}
	return str.String()
}

// Return grid width and height.
func (g *Grid[T]) Dimensions() (x, y int) {
	x, y = g.width, g.height
	return
}
