package ds

// Identifier for graph nodes.
type NodeKey int

// Node is the interface of a graph node. See GraphNode for a generic implementation.
type Node interface {
	Key() NodeKey
	// Gets the neighbours of node
	Neighbours() []Node
	// Cost of going to node. Return negative cost if Node is not a neighbour
	Cost(Node) int
}

// A generic implementation of Node. GraphNodes can hold any data. The transition weights (costs)
// are positive ints.
type GraphNode[T any] struct {
	K          NodeKey
	Data       T
	neighbours []*GraphNode[T]
	costs      map[NodeKey]int
}

func NewGraphNode[T any](data T, key NodeKey) *GraphNode[T] {
	return &GraphNode[T]{
		Data:  data,
		K:     key,
		costs: map[NodeKey]int{},
	}
}

func (g *GraphNode[T]) Key() NodeKey {
	return g.K
}

// Get neighbours of node.
func (g *GraphNode[T]) Neighbours() []Node {
	n := []Node{}
	for i := range g.neighbours {
		n = append(n, g.neighbours[i])
	}
	return n
}

// Cost of moving into this node.
func (g *GraphNode[T]) Cost(node Node) int {
	if v, ok := g.costs[node.Key()]; ok {
		return v
	}
	return -1
}

// Set node as neighbour of current node.
func (g *GraphNode[T]) LinkTo(node *GraphNode[T], cost int) {
	g.neighbours = append(g.neighbours, node)
	g.costs[node.Key()] = cost
}

// Set node as neighbour of current node and current node as neighbour of node.
func (g *GraphNode[T]) LinkBidirectional(node *GraphNode[T], costTo, costBack int) {
	g.neighbours = append(g.neighbours, node)
	g.costs[node.Key()] = costTo
	node.neighbours = append(node.neighbours, g)
	node.costs[g.K] = costBack
}
