package ds

import (
	"testing"
)

type heapInt int

func (a heapInt) Priority() int {
	return int(a)
}

func TestHeapIndexing(t *testing.T) {
	heap := Heap[heapInt]{}
	parent := []int{0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4}
	for i := 0; i < 10; i++ {
		if heap.parent(i) != parent[i] {
			t.Error(heap.parent(i), parent[i])
		}
	}
}

func TestHeapPushPop(t *testing.T) {
	heap := Heap[heapInt]{}
	values := []heapInt{12, 13, 16, 1, 2, 6, 4, 100, 2}
	for i := range values {
		heap.Push(values[i])
	}
	for i := 95; i < 105; i++ {
		heap.Push(heapInt(i))
	}

	if heap.PeekTop() != 104 {
		t.Error(heap)
	}
	heap.Pop()

	pops := []int{103, 102, 101, 100, 100, 99, 98, 97, 96, 95, 16, 13, 12, 6, 4, 2, 2, 1}
	for i := range pops {
		if val := heap.Pop(); int(val) != pops[i] {
			t.Fatal(i, val, pops[i])
		}
	}

	for i := range pops {
		heap.Push(heapInt(pops[i]))
	}
	heap.SetMinHeapMode()

	reversePops := []int{}
	for i := len(pops) - 1; i >= 0; i-- {
		reversePops = append(reversePops, pops[i])
	}

	for i := range reversePops {
		if val := heap.Pop(); int(val) != reversePops[i] {
			//t.Fatal(i, val, pops[i])
		}
	}

	minheap := NewHeap(values, true)
	for i := range values {
		minheap.Push(values[i])
	}
	expect := []int{1, 1, 2, 2, 2, 2, 4, 4, 6, 6, 12, 12, 13, 13, 16, 16, 100, 100}
	for i := range expect {
		if val := minheap.Pop(); int(val) != expect[i] {
			t.Error(val, expect[i])
		}
	}
}

func TestHeapPushPopDups(t *testing.T) {
	heap := Heap[heapInt]{}
	heap.SetMinHeapMode()
	data := []heapInt{10, 10, 10, 20, 20}
	for i := range data {
		heap.Push(data[i])
	}
	for i := range data {
		if d := heap.Pop(); d != data[i] {
			t.Error(d, data)
		}
	}
}

func TestHeapBuild(t *testing.T) {
	data := []heapInt{12, 63, 1, 5, 7, 70, 11}
	maxheap := NewHeap(data, false)
	if val := maxheap.Pop(); val != 70 {
		t.Error(val)
	}

	minheap := NewHeap(data, true)
	if val := minheap.Pop(); val != 1 {
		t.Error(val)
	}
}
