package ds

import (
	"gitlab.com/onikolas/math"
)

// A binary heap implemented with an array. It is a max heap by default, but can be changed to a min
// heap. Switching can happen in place if needed, at cost O(n). It accepts data that implements the
// HeapNode interface.
type Heap[T HeapNode] struct {
	data    []T
	next    int
	minHeap bool
}

// Data going in Heap must implement Priority() which lets the Heap decide which element is on top.
type HeapNode interface {
	Priority() int
}

// parent of node at i
func (h *Heap[T]) parent(i int) int {
	return (i - 1) / 2
}

// children of node at i
func (h *Heap[T]) children(i int) (int, int) {
	return 2*i + 1, 2*i + 2
}

func (h *Heap[T]) swap(i, j int) {
	h.data[i], h.data[j] = h.data[j], h.data[i]
}

// does a have priority over b
func (h *Heap[T]) priority(a, b int) bool {
	if h.minHeap {
		return h.data[a].Priority() < h.data[b].Priority()
	}
	return h.data[a].Priority() > h.data[b].Priority()
}

func (h *Heap[T]) resize() {
	// double the size
	if h.data == nil {
		h.data = make([]T, 16)
		return
	}

	newData := make([]T, len(h.data)*2)
	copy(newData, h.data)
	h.data = newData
}

// Push an item to the heap.
func (h *Heap[T]) Push(val T) {
	if h.next == len(h.data) {
		h.resize()
	}

	h.data[h.next] = val

	for i := h.next; i > 0; i = h.parent(i) {
		parent := h.parent(i)
		if h.priority(i, parent) {
			h.swap(i, parent)
		} else {
			break
		}
	}

	h.next++
}

// Remove the item at the top of the heap.
func (h *Heap[T]) Pop() T {
	if h.next == 0 {
		var zero T
		return zero
	}

	top := h.data[0]
	h.data[0] = h.data[h.next-1]
	h.next--
	h.fixDown(0)
	return top
}

// Is the Heap empty?
func (h *Heap[T]) IsEmpty() bool {
	return h.next == 0
}

// Starting at index, check that the subtree below is a heap and fix if needed.
func (h *Heap[T]) fixDown(index int) {
	for i := index; ; {
		left, right := h.children(i)
		prio := i

		if left < h.next && h.priority(left, prio) {
			prio = left
		}

		if right < h.next && h.priority(right, prio) {
			prio = right
		}

		if prio != i {
			h.swap(i, prio)
			i = prio
		} else {
			break // order restored once a swap is not needed
		}
	}
}

// Return the top of the heap without removing it.
func (h *Heap[T]) PeekTop() T {
	return h.data[0]
}

// Min priority at the top of the heap. Will reorganize elements if needed.
func (h *Heap[T]) SetMinHeapMode() {
	h.minHeap = true
	h.build()
}

// Max priority at the top of the heap. Will reorganize elements if needed. Max is the default.
func (h *Heap[T]) SetMaxHeapMode() {
	h.minHeap = false
	h.build()
}

// Build the heap using Floyd's method which is O(n) due to some dark magic.
func (h *Heap[T]) build() {
	// start from the first index on the second to last layer (leaf nodes don't need fixing down)
	l2 := math.NextPowerOf2(uint64(h.next-1)) / 2
	fixdowns := 0
	for i := int(l2); i >= 0; i-- {
		h.fixDown(i)
		fixdowns++
	}
}

// Creates a new Heap of type T.
func NewHeap[T HeapNode](data []T, minHeap bool) *Heap[T] {
	heap := Heap[T]{}
	if minHeap {
		heap.SetMinHeapMode()
	}
	heap.next = len(data)
	heap.data = make([]T, len(data))
	copy(heap.data, data)
	heap.build()
	return &heap
}
