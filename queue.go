package ds

import (
	"errors"

	"gitlab.com/onikolas/math"
)

// FIFO Q
type Queue[T any] struct {
	data        []T
	out, in     int
	resizeChunk int
}

const QueueDefaultSize = 16

// Initialize q to a size. Will delete existing data.
func (q *Queue[T]) Init(size int) {
	if size <= 0 {
		size = QueueDefaultSize
	}
	q.data = make([]T, size)
}

// Add to the q.
func (q *Queue[T]) Push(data T) {
	if q.data == nil {
		q.Init(QueueDefaultSize)
	}
	q.data[q.in] = data

	if q.next(q.in) == q.out {
		q.resize()
	} else {
		q.in = q.next(q.in)
	}
}

// Get and remove last oldest element.
func (q *Queue[T]) Pop() (T, error) {
	if q.in == q.out {
		var zero T
		return zero, errors.New("Queue empty")
	}

	data := q.data[q.out]
	q.out = q.next(q.out)
	return data, nil
}

// Length of data in the q.
func (q *Queue[T]) DataLength() int {
	if q.in >= q.out {
		return q.in - q.out
	}
	return len(q.data) - (q.out - q.in)
}

// How much to increase the q by when it runs out of space.
func (q *Queue[T]) SetResizeChunk(i int) {
	q.resizeChunk = math.Abs(i)
}

func (q *Queue[T]) next(i int) int {
	return (i + 1) % len(q.data)
}

func (q *Queue[T]) resize() {
	if q.resizeChunk == 0 {
		q.resizeChunk = 16
	}

	newData := make([]T, len(q.data)+q.resizeChunk)
	ndi := 0
	for i := q.out; ; i = q.next(i) {
		newData[ndi] = q.data[i]
		ndi++
		if i == q.in {
			break
		}
	}
	q.data = newData
	q.out = 0
	q.in = ndi
}
