package ds

// mappedSliceRange stores an index into a slice. To be used like this: slice[Start:End]
type mappedSliceRange struct {
	Start, End int
}

// MappedSlice is an indexed slice. It can be used to retrieve specific sub-portions of the slice
// without searching. Can be used in cases where map-like access is required (where a map would be
// appropriate) and the whole slice is often iterated upon (where a slice is faster than a map). Can
// also be used to store multiple small slices in a continuous piece of memory.
type MappedSlice[ST any, MT comparable] struct {
	S []ST
	M map[MT]mappedSliceRange
}

func (i MappedSlice[ST, MT]) New() MappedSlice[ST, MT] {
	i.M = map[MT]mappedSliceRange{}
	return i
}

// Get slice portion indexed with key.
func (i MappedSlice[ST, MT]) Get(key MT) []ST {
	start, end := i.M[key].Start, i.M[key].End
	return i.S[start:end]
}

// Add data and index it with key.
func (i *MappedSlice[ST, MT]) AddMapped(data ST, key MT) {
	i.S = append(i.S, data)
	i.M[key] = mappedSliceRange{len(i.S) - 1, len(i.S)}
}

// Add a data slice and index it with a key.
func (i *MappedSlice[ST, MT]) AddSliceMapped(data []ST, key MT) {
	i.S = append(i.S, data...)
	i.M[key] = mappedSliceRange{len(i.S) - len(data), len(i.S)}
}
