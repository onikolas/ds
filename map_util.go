package ds

import (
	"sort"

	"golang.org/x/exp/constraints"
)

// Get the keys of a map sorted by key value
func GetMapKeysSorted[K constraints.Ordered, V any](m map[K]V) []K {
	keys := make([]K, 0, len(m))
	for k, _ := range m {
		keys = append(keys, k)
	}
	sort.Slice(keys, func(i, j int) bool {
		return keys[i] < keys[j]
	})
	return keys
}

// Get the keys of a map sorted by the map value. Provide a function to do the value sort test.
func SortMapByValue[K constraints.Ordered, V any](m map[K]V, sortFn func(a, b V) bool) []K {
	type kv struct {
		key K
		val V
	}
	kvs := make([]kv, 0, len(m))

	for k, v := range m {
		kvs = append(kvs, kv{key: k, val: v})
	}

	sort.Slice(kvs, func(i, j int) bool {
		return sortFn(kvs[i].val, kvs[j].val)
	})

	keys := make([]K, 0, len(m))
	for i := range kvs {
		keys = append(keys, kvs[i].key)
	}

	return keys
}
