package ds

import (
	"testing"
)

func TestGrid(t *testing.T) {
	grid := NewGrid[int](12, 8)
	grid.Set(5, 6, 5)
	grid.SetCost(3, 4, (1))
	grid.SetCost(4, 5, (3))
	grid.SetCost(5, 5, (7))
	grid.SetCost(5, 6, (5))
	grid.SetCost(4, 4, (4))
	grid.SetCost(4, 3, (9))
	grid.SetNeighbourHoodMode8(1.41)

	if grid.Get(5, 6) != 5 {
		t.Error("!")
	}

	n := grid.GetNode(4, 5)
	nei := n.Neighbours()
	if len(nei) != 8 {
		t.Error(nei)
	}

	grid.SetNeighbourHoodMode4()
	n = grid.GetNode(4, 5)
	nei = n.Neighbours()
	if len(nei) != 4 {
		t.Error(nei)
	}

	if cost := nei[1].Cost(&n); cost != 3 {
		t.Error(cost)
	}

	if cost := n.Cost(nei[1]); cost != 7 {
		t.Error(cost)
	}

	n = grid.GetNode(11, 0)
	nei = n.Neighbours()
	if len(nei) != 2 {
		t.Error(nei)
	}

	grid.SetNeighbourHoodMode8(1.41)
	n = grid.GetNode(11, 7)
	nei = n.Neighbours()
	if len(nei) != 3 {
		t.Error(nei)
	}
}
