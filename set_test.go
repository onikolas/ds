package ds

import (
	"testing"
)

func TestSet(t *testing.T) {
	set := NewSet[string]()
	set.Add("Nikolas")
	set.Add("nik")
	if set.Has("nikolas") {
		t.Error(set)
	}
	if !set.Has("Nikolas") {
		t.Error(set)
	}
	if !set.Has("nik") {
		t.Error(set)
	}
	set.Remove("nik")
	if set.Has("nik") {
		t.Error(set)
	}
}
