package ds

import (
	"testing"
)

func TestStack(t *testing.T) {
	s := Stack{}

	pushes := []interface{}{0, 10, "asd"}
	pops := []interface{}{"asd", 10, 0}

	for _, v := range pushes {
		s.Push(v)
	}
	for _, v := range pops {
		if res := s.Pop(); res != v {
			t.Error(res)
		}
	}

	// empty case
	if res := s.Pop(); res != nil {
		t.Error(res)
	}

	// shrinkage
	for i := 0; i < 10; i++ {
		s.Push(i * 10)
	}
	for i := 0; i < 8; i++ {
		s.Pop()
	}
	s.Shrink()
	if len(s.data) != 2 {
		t.Error(s, len(s.data))
	}

	if s.Peek() != 10 {
		t.Error(s)
	}
	if s.Peek() != 10 {
		t.Error(s)
	}
}
