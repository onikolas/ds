package ds

import (
	"testing"
)

func TestQueue(t *testing.T) {
	q := Queue[string]{}
	if dl := q.DataLength(); dl != 0 {
		t.Error(dl)
	}
	q.Push("12")
	q.Push("13")
	if dl := q.DataLength(); dl != 2 {
		t.Error(dl)
	}

	if data, err := q.Pop(); data != "12" || err != nil {
		t.Error(data, q, err)
	}
	if data, err := q.Pop(); data != "13" || err != nil {
		t.Error(data, q, err)
	}
	if _, err := q.Pop(); err == nil {
		t.Error(q, err)
	}

	intq := Queue[int]{}
	intq.Init(4)
	intq.SetResizeChunk(4)
	for i := 0; i < 6; i++ {
		intq.Push(i)
	}
	if dl := intq.DataLength(); dl != 6 {
		t.Error(dl, intq)
	}

	for i := 0; i < 4; i++ {
		if v, err := intq.Pop(); v != i || err != nil {
			t.Error(v, err)
		}
	}

	for i := 66; i < 109; i++ {
		intq.Push(i)
	}

	for i := 4; i < 6; i++ {
		if v, err := intq.Pop(); v != i || err != nil {
			t.Error(v, err)
		}
	}

	for i := 66; i < 109; i++ {
		if v, err := intq.Pop(); v != i || err != nil {
			t.Error(v, err)
		}
	}

	if dl := intq.DataLength(); dl != 0 {
		t.Error(dl, intq)
	}
}
