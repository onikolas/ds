package ds

// A map implementation of a set. Important set operations are currently missing (intersect, diff, union).
type Set[T comparable] struct {
	m map[T]bool
}

// Initialize a new set.
func NewSet[T comparable]() *Set[T] {
	return &Set[T]{
		m: map[T]bool{},
	}
}

// Add item to the set.
func (s *Set[T]) Add(item T) {
	s.m[item] = true
}

// Is item in the set?
func (s *Set[T]) Has(item T) bool {
	if v, ok := s.m[item]; ok && v {
		return true
	}
	return false
}

// Remove item from set. Remove is a O(1) operation but does not release used memory.
func (s *Set[T]) Remove(item T) {
	s.m[item] = false
}

// Same as Cardinality.
func (s *Set[T]) Length() int {
	i := 0
	for _, v := range s.m {
		if v {
			i++
		}
	}
	return i
}

// Number of items it the set.
func (s *Set[T]) Cardinality() int {
	return s.Length()
}
