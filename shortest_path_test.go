package ds

import (
	"fmt"
	"strings"
	"testing"

	"gitlab.com/onikolas/math"
)

func buildTestGraph() *GraphNode[string] {

	a := NewGraphNode("a", 1)
	b := NewGraphNode("b", 2)
	c := NewGraphNode("c", 3)
	d := NewGraphNode("d", 4)
	e := NewGraphNode("e", 5)
	f := NewGraphNode("f", 6)
	g := NewGraphNode("g", 7)
	h := NewGraphNode("h", 8)
	i := NewGraphNode("i", 9)

	a.LinkBidirectional(b, 3, 3)
	a.LinkBidirectional(e, 7, 7)
	a.LinkBidirectional(g, 5, 5)
	b.LinkBidirectional(c, 7, 7)
	b.LinkBidirectional(e, 1, 1)
	c.LinkBidirectional(d, 1, 1)
	c.LinkBidirectional(f, 2, 2)
	c.LinkBidirectional(e, 2, 2)
	d.LinkBidirectional(f, 3, 3)
	d.LinkBidirectional(i, 5, 5)
	e.LinkBidirectional(f, 1, 1)
	e.LinkBidirectional(g, 3, 3)
	e.LinkBidirectional(h, 3, 3)
	f.LinkBidirectional(h, 3, 3)
	f.LinkBidirectional(i, 2, 2)
	g.LinkBidirectional(h, 2, 2)
	h.LinkBidirectional(i, 4, 4)

	return a
}

func checkEq(a, b []NodeKey) bool {
	if len(a) != len(b) {
		return false
	}
	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}

var heuristic = func(a, b Node) int {
	na := a.(*GridNode[int])
	nb := b.(*GridNode[int])
	va, vb := math.Vector2[int]{na.x, na.y}, math.Vector2[int]{nb.x, nb.y}
	return math.ManhattanDistance(va, vb) * 10
}

func TestPathSearchAll(t *testing.T) {
	start := buildTestGraph()
	paths := PathSearchAll(start)

	if path := paths.PathTo(1); !checkEq(path, []NodeKey{1}) {
		t.Error(path)
	}
	if path := paths.PathTo(2); !checkEq(path, []NodeKey{2}) {
		t.Error(path)
	}
	if path := paths.PathTo(3); !checkEq(path, []NodeKey{2, 5, 3}) {
		t.Error(path)
	}
	if path := paths.PathTo(4); !checkEq(path, []NodeKey{2, 5, 3, 4}) {
		t.Error(path)
	}
	if path := paths.PathTo(5); !checkEq(path, []NodeKey{2, 5}) {
		t.Error(path)
	}
	if path := paths.PathTo(6); !checkEq(path, []NodeKey{2, 5, 6}) {
		t.Error(path)
	}
	if path := paths.PathTo(7); !checkEq(path, []NodeKey{7}) {
		t.Error(path)
	}
	if path := paths.PathTo(8); !checkEq(path, []NodeKey{2, 5, 8}) {
		t.Error(path)
	}
	if path := paths.PathTo(9); !checkEq(path, []NodeKey{2, 5, 6, 9}) {
		t.Error(path)
	}
}

func TestPathSearch(t *testing.T) {
	start := buildTestGraph()

	h := func(a, b Node) int {
		return 12
	}

	if end := NewGraphNode("doesn't matter", 1); !checkEq(PathSearch(start, end, h), []NodeKey{1}) {
		t.Error(PathSearch(start, end, h))
	}
	if end := NewGraphNode("doesn't matter", 2); !checkEq(PathSearch(start, end, PathSearchNoHeuristic), []NodeKey{2}) {
		t.Error(PathSearch(start, end, h))
	}
	if end := NewGraphNode("doesn't matter", 3); !checkEq(PathSearch(start, end, h), []NodeKey{2, 5, 3}) {
		t.Error(PathSearch(start, end, h))
	}
	if end := NewGraphNode("doesn't matter", 4); !checkEq(PathSearch(start, end, h), []NodeKey{2, 5, 3, 4}) {
		t.Error(PathSearch(start, end, h))
	}
	if end := NewGraphNode("doesn't matter", 5); !checkEq(PathSearch(start, end, h), []NodeKey{2, 5}) {
		t.Error(PathSearch(start, end, h))
	}
	if end := NewGraphNode("doesn't matter", 6); !checkEq(PathSearch(start, end, h), []NodeKey{2, 5, 6}) {
		t.Error(PathSearch(start, end, h))
	}
	if end := NewGraphNode("doesn't matter", 7); !checkEq(PathSearch(start, end, h), []NodeKey{7}) {
		t.Error(PathSearch(start, end, h))
	}
	if end := NewGraphNode("doesn't matter", 8); !checkEq(PathSearch(start, end, PathSearchNoHeuristic), []NodeKey{2, 5, 8}) {
		t.Error(PathSearch(start, end, h))
	}
	if end := NewGraphNode("doesn't matter", 9); !checkEq(PathSearch(start, end, PathSearchNoHeuristic), []NodeKey{2, 5, 6, 9}) {
		t.Error(PathSearch(start, end, h))
	}
	if end := NewGraphNode("doesn't matter", 9); !checkEq(PathSearch(start, end, h), []NodeKey{2, 5, 6, 9}) {
		t.Error(PathSearch(start, end, h))
	}
}

func TestSearchGrid(t *testing.T) {

	grid := NewGrid[int](9, 7)
	grid.SetAllCosts(10)
	grid.SetCost(4, 2, (99))
	grid.SetCost(3, 2, (99))
	grid.SetCost(2, 2, (99))
	grid.SetCost(2, 3, (99))
	grid.SetCost(2, 4, (99))
	grid.SetCost(2, 5, (99))
	grid.SetCost(2, 6, (99))

	// S:start, E:end, o:path
	// . . X . . . . . .
	// . . X . . . . . .
	// . . X . . . . . .
	// . . X E o . . . .
	// . . X X X o . . .
	// S o o o o . . . .
	// . . . . . . . . .
	expectedPath := []math.Vector2[int]{
		{1, 1}, {2, 1}, {3, 1}, {4, 1}, {5, 2}, {4, 3}, {3, 3},
	}

	grid.SetNeighbourHoodMode8(1.41)

	start := grid.GetNode(0, 1)
	end := grid.GetNode(3, 3)
	path := PathSearch(&start, &end, PathSearchNoHeuristic)
	if len(expectedPath) != len(path) {
		for i := range path {
			x, y := grid.deIndex(int(path[i]))
			fmt.Printf("(%v, %v)", x, y)
		}
		t.Fatal(path)
	}
	for i := range path {
		x, y := grid.deIndex(int(path[i]))
		v := math.Vector2[int]{x, y}
		if expectedPath[i] != v {
			t.Error(i, expectedPath[i], v)
			for i := range path {
				x, y := grid.deIndex(int(path[i]))
				fmt.Printf("(%v, %v)", x, y)
			}
		}
	}

	// same thing but corner transitions are included
	// . . X E o o . . .
	// . . X X X o . . .
	// S o o o o o . . .
	// . . . . . . . . .
	grid.SetNeighbourHoodMode4()
	start = grid.GetNode(0, 1)
	end = grid.GetNode(3, 3)
	path = PathSearch(&start, &end, heuristic)

	expectedPath = []math.Vector2[int]{
		{1, 1}, {2, 1}, {3, 1}, {4, 1}, {5, 1}, {5, 2}, {5, 3}, {4, 3}, {3, 3},
	}
	for i := range path {
		x, y := grid.deIndex(int(path[i]))
		v := math.Vector2[int]{x, y}

		if expectedPath[i] != v {
			t.Error(i, expectedPath[i], v)
		}
	}

	// add a shortcut
	// . . X E . . . . .
	// . . X o X . . . .
	// S o o o . . . . .
	// . . . . . . . . .
	grid.SetCost(3, 2, 3)
	path = PathSearch(&start, &end, PathSearchNoHeuristic)
	expectedPath = []math.Vector2[int]{
		{1, 1}, {2, 1}, {3, 1}, {3, 2}, {3, 3},
	}
	for i := range path {
		x, y := grid.deIndex(int(path[i]))
		v := math.Vector2[int]{x, y}
		if expectedPath[i] != v {
			t.Error(i, expectedPath[i], v)
		}
	}
}

// Visualization of search
func TestSearchGrid2(t *testing.T) {
	grid := NewGrid[int](15, 15)
	grid.SetAllCosts(10)
	grid.SetNeighbourHoodMode8(1.41)
	for i := 2; i < 13; i++ {
		grid.SetBlocked(i, 2)
	}
	for i := 2; i < 13; i++ {
		grid.SetBlocked(12, i)
	}
	for i := 5; i < 13; i++ {
		grid.SetBlocked(i, 12)
	}

	start := grid.GetNode(0, 2)
	end := grid.GetNode(11, 13)
	path := PathSearch(&start, &end, heuristic)
	if len(path) == 0 {
		t.Error(path)
	}
	if testing.Verbose() {
		PrintPath(math.Vector2[int]{start.x, start.y}, grid, path)
	}
}

func PrintPath(start math.Vector2[int], g *Grid[int], path []NodeKey) {
	str := strings.Builder{}
	for y := g.height - 1; y >= 0; y-- {
		for x := 0; x < g.width; x++ {
			found := false
			for i := range path {
				px, py := g.deIndex(int(path[i]))
				if px == x && py == y {
					fmt.Fprint(&str, " o", " ")
					found = true
				}
			}
			ss := math.Vector2[int]{X: x, Y: y}

			if !found {
				if g.IsBlocked(x, y) {
					fmt.Fprint(&str, " X ")
				} else if ss == start {
					fmt.Fprint(&str, " S ")
				} else {
					fmt.Fprint(&str, " . ")
				}
			}

		}
		fmt.Fprintln(&str)
	}
	fmt.Println(str.String())
}
