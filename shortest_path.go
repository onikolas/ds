package ds

import (
	"math"
)

type edge struct {
	node Node
	cost int
}

func (e *edge) Priority() int {
	return e.cost
}

// Paths is the result of running PathSearchAll and stores all paths starting at node Start. To get
// a path to a specific destination use PathTo.
type Paths struct {
	Start NodeKey
	paths map[NodeKey]NodeKey
}

// Get the path to a specific node.
func (p Paths) PathTo(node NodeKey) []NodeKey {
	_, ok := p.paths[node]
	if !ok {
		return nil
	}
	path := []NodeKey{}
	n := node
	for i := 0; i < len(p.paths); i++ {
		path = append(path, n)
		n = p.paths[n]
		if n == p.Start {
			break
		}
	}

	// reverse it
	for i, j := 0, len(path)-1; i < j; i, j = i+1, j-1 {
		path[i], path[j] = path[j], path[i]
	}
	return path
}

// Search all paths starting at node. Uses Dijkstra's method. Returns a Paths object which contains
// a map with destination->source entries. Use Paths.PathTo() to get paths from start to specific
// nodes.
func PathSearchAll(start Node) Paths {
	// current best distance to node
	distance := map[NodeKey]int{start.Key(): 0}
	// keeps track of already visited nodes
	visited := NewSet[NodeKey]()
	// tracks paths taken
	paths := map[NodeKey]NodeKey{}
	paths[start.Key()] = start.Key()
	// priority list of nodes to explore next (highest priority=smallest cost)
	next := Heap[*edge]{}
	next.Push(&edge{node: start, cost: 0})
	next.SetMinHeapMode()

	for !next.IsEmpty() {
		node := next.Pop().node
		if visited.Has(node.Key()) {
			continue
		}
		visited.Add(node.Key())
		neighbours := node.Neighbours()

		for i := range neighbours {
			neighbour := neighbours[i]
			costToNeighbour := node.Cost(neighbour)
			newCost := distance[node.Key()] + costToNeighbour
			existingCost, hasCost := distance[neighbour.Key()]

			if !visited.Has(neighbour.Key()) {
				next.Push(&edge{
					node: neighbour,
					cost: newCost,
				})
			}

			if !hasCost || newCost < existingCost {
				distance[neighbour.Key()] = newCost
				paths[neighbour.Key()] = node.Key()
			}
		}
	}

	return Paths{
		paths: paths,
		Start: start.Key(),
	}
}

// Used in PathSearch in place of an actual heuristic function. Returns 0.
func PathSearchNoHeuristic(a, b Node) int { return 0 }

// Search for a path to a specific node. Faster than PathSearchAll since it can exit as soon as it
// reaches the destination. Pass a heuristic funcion to prioritize node evaluation. For example, on
// a grid, the heuristic can be the Manhattan distance of the two nodes which turns PathSearch into
// A*. Pass PathSearchNoHeuristic to do Dijkstra with early exit.
func PathSearch(start, end Node, heuristic func(a, b Node) int) []NodeKey {
	if start.Key() == end.Key() {
		return []NodeKey{start.Key()}
	}

	// current best distance to nodey
	distance := map[NodeKey]int{start.Key(): 0, end.Key(): math.MaxInt}
	// keeps track of already visited nodes
	visited := NewSet[NodeKey]()
	// tracks paths taken
	paths := map[NodeKey]NodeKey{}
	paths[start.Key()] = start.Key()
	// priority list of nodes to explore next (highest priority=smallest cost)
	next := Heap[*edge]{}
	next.Push(&edge{node: start, cost: 0})
	next.SetMinHeapMode()

	for !next.IsEmpty() {
		node := next.Pop().node
		if visited.Has(node.Key()) {
			continue
		}
		if node.Key() == end.Key() {
			break
		}
		visited.Add(node.Key())
		neighbours := node.Neighbours()

		for i := range neighbours {
			neighbour := neighbours[i]
			costToNeibour := node.Cost(neighbour)
			newCost := distance[node.Key()] + costToNeibour
			existingCost, hasCost := distance[neighbour.Key()]

			// push to frontier if not visited
			if !visited.Has(neighbour.Key()) {
				next.Push(&edge{
					node: neighbour,
					cost: newCost + heuristic(neighbour, end),
				})
			}

			if !hasCost || newCost < existingCost {
				distance[neighbour.Key()] = newCost
				paths[neighbour.Key()] = node.Key()
			}
		}
	}

	return Paths{Start: start.Key(), paths: paths}.PathTo(end.Key())
}
